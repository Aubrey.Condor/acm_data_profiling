#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Feb 22 12:41:35 2023

@author: mzft7n
"""

# only value, and MAYBE updated time. 
import datetime
from datetime import date, datetime, timezone
import pandas as pd
from dateutil.parser import parse

import cx_Oracle


class AltID():
    """
    A class used to validate the individual entity.
    """
    
    def __init__(
        self, 
        thirdpartyid_column_name: str = "THIRDPARTYID"
    ) -> None:        

        self.thirdpartyid_column_name = thirdpartyid_column_name
        
        self.sql_str = "With tbl1 as (select a.agrmnt_id from IODS.IODS_SCHED_DERIVED_ATTR_V  a \
        where a.AGRMNT_STATUS in ('Active', 'TerminationInProgress') and a.agrmnt_id not in (1,2)), \
            tbl2 as (select a.agrmnt_id, b.EXPCUSID, b.INVCUSID from tbl1 a, ALFA_ODS.odsAgreement b where a.agrmnt_id = b.id), \
                tbl3 as (select distinct b.AGREEMENTID, b.GUARANTORID from tbl1 a, ALFA_ODS.odsSecurity b where a.agrmnt_id = b.AGREEMENTID \
                         and b.SECURITYTYPENAME in ('Guarantor','Cobuyer','Colessee','Spouse','Owner','Other Owner','Previous Buyer',\
                          'Previous Co-Buyer','Line of Credit Guarantor','Previous Co-Lessee','Previous Guarantor','Previous Lessee')), \
        tbl4 as (select distinct agrmnt_id, EXPCUSID as ThirdPartyId,1 from tbl2 union \
                 select distinct agrmnt_id, INVCUSID,2 from tbl2 union \
                     select AGREEMENTID, GUARANTORID,3 from tbl3) \
        select distinct thirdpartyid \
        from  (Select b.agrmnt_id, b.ThirdPartyId, a.THIRDPARTYTYPE, a.NATIONALIDNUMBER, c.employername, c.LASTNAME \
        from alfa_ods.odsThirdParty a, tbl4 b, alfa_ods.odsBillingAddress c \
        where b.ThirdPartyId = a.id \
         and  b.ThirdPartyId = c.THIRDPARTYID \
         and c.BILLINGNUMBER = 1 \
         and a.thirdpartytype = '1012')"


    def thirdpartyid(self, thirdpartyid: str) -> str:
        """ line 31
        Alt ID cannot be empty.

        """
        
        entity = 'alternate id'
        line= '31'
        
        if len(thirdpartyid) == 0:
            return entity + ' ' + line + ' - third party id empty'
        else:
            return ""
   
        
    def run(self):
        
        cx_Oracle.init_oracle_client(lib_dir='instantclient_19_8')
        # connect to IODSP
        self.or_con = cx_Oracle.connect(
                            'MZFT7N',
                            'Wxzy*11251991',
                            '{}:{}/{}'.format('dfw1lepdb421-scan.ally.corp','2030','IODSP',
                            encoding='UTF-8'))
        
        cur = self.or_con.cursor()
        
        cur.execute(self.sql_str)
        column_names = [x[0] for x in cur.description]
        num_rows = 10000
        self.violations_df = pd.DataFrame()
        
        while True:    
            rows = cur.fetchmany(num_rows)
            if not rows:
                break    
            df=pd.DataFrame(rows)
            df.columns = column_names

            df['thirdpartyid_obj'] = df[self.thirdpartyid_column_name].apply(lambda x: str(x).strip())
            
            df['thirdpartyid_violations'] = df.apply(lambda x: self.thirdpartyid(x['thirdpartyid_obj']), axis=1)
    
            df_violations = df[(df['thirdpartyid_violations'] != "")]
            

            self.violations_df = pd.concat([self.violations_df, df_violations], axis=0)
            print(len(self.violations_df))
        
                
if __name__ == "__main__":
    AltID_Validation = AltID()
    
    AltID_Validation.run()
    AltID_Validation.violations_df.to_csv('AltID_Violations.csv')


