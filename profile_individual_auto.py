"""
Created on Fri Dec 23 2022

@author: vzywf2
"""

import datetime
from datetime import date, datetime, timezone
import pandas as pd
from dateutil.parser import parse

import cx_Oracle


class Individual_Entity_Validation():
    """
    A class used to validate the individual entity.
    """
    
    def __init__(
        self, 
        birth_date_column_name: str = "DATEOFBIRTH",
        death_date_column_name: str = "DECEASED_DT"
    ) -> None:        

        self.birth_date_column_name = birth_date_column_name
        self.death_date_column_name = death_date_column_name
        
        self.sql_str_dob = "With tbl1 as (select a.agrmnt_id from IODS.IODS_SCHED_DERIVED_ATTR_V  a \
        where a.AGRMNT_STATUS in ('Active', 'TerminationInProgress') and a.agrmnt_id not in (1,2)), \
            tbl2 as (select a.agrmnt_id, b.EXPCUSID, b.INVCUSID from tbl1 a, ALFA_ODS.odsAgreement b where a.agrmnt_id = b.id), \
                tbl3 as (select distinct b.AGREEMENTID, b.GUARANTORID from tbl1 a, ALFA_ODS.odsSecurity b where a.agrmnt_id = b.AGREEMENTID \
                         and b.SECURITYTYPENAME in ('Guarantor','Cobuyer','Colessee','Spouse','Owner','Other Owner','Previous Buyer',\
                          'Previous Co-Buyer','Line of Credit Guarantor','Previous Co-Lessee','Previous Guarantor','Previous Lessee')), \
        tbl4 as (select distinct agrmnt_id, EXPCUSID as ThirdPartyId,1 from tbl2 union \
                 select distinct agrmnt_id, INVCUSID,2 from tbl2 union \
                     select AGREEMENTID, GUARANTORID,3 from tbl3) \
        select distinct thirdpartyid, nationalidnumber, dateofbirth, lastname \
        from  (Select b.agrmnt_id, b.ThirdPartyId, a.THIRDPARTYTYPE, a.NATIONALIDNUMBER, c.DATEOFBIRTH, c.LASTNAME \
        from alfa_ods.odsThirdParty a, tbl4 b, alfa_ods.odsBillingAddress c \
        where b.ThirdPartyId = a.id \
         and  b.ThirdPartyId = c.THIRDPARTYID \
         and c.BILLINGNUMBER = 1 \
         and a.thirdpartytype = '1012')"
            
        self.sql_str_dod = "With tbl1 as (select a.agrmnt_id from IODS.IODS_SCHED_DERIVED_ATTR_V a \
                                    where a.AGRMNT_STATUS in ('Active', 'TerminationInProgress') and a.agrmnt_id not in (1,2)), \
                            tbl2 as (Select a.agrmnt_id, b.EXPCUSID, b.INVCUSID from tbl1 a, ALFA_ODS.odsAgreement b \
                                     where a.agrmnt_id = b.id),\
                            tbl3 as (Select distinct b.AGREEMENTID, b.GUARANTORID from tbl1 a, ALFA_ODS.odsSecurity b \
                                     where a.agrmnt_id = b.AGREEMENTID and b.SECURITYTYPENAME in ( 'Guarantor','Cobuyer','Colessee', \
                                                                                                  'Spouse','Owner','Other Owner','Previous Buyer', \
                                                                                                  'Previous Co-Buyer','Line of Credit Guarantor',\
                                                                                                  'Previous Co-Lessee','Previous Guarantor', \
                                                                                                  'Previous Lessee')), \
                            tbl4 as (Select distinct agrmnt_id, EXPCUSID as ThirdPartyId,1 from tbl2 union \
                                     Select distinct agrmnt_id, INVCUSID,2  from tbl2 union \
                                     Select AGREEMENTID, GUARANTORID,3 from tbl3) \
                            Select distinct ThirdPartyId, dateofbirth, deceased_dt \
                                from  (Select b.agrmnt_id, b.ThirdPartyId, a.THIRDPARTYTYPE, a.NATIONALIDNUMBER, \
                                       c.DATEOFBIRTH, c.LASTNAME, d.deceased_dt \
                                           from alfa_ods.odsThirdParty a, tbl4 b, alfa_ods.odsBillingAddress c, \
                                               ALFA_ODS.ODSTHIRDPARTYMISCELLANEOUS d \
                                                   where b.ThirdPartyId = a.id and  b.ThirdPartyId = c.THIRDPARTYID \
                                                       and b.ThirdPartyId = d.ThirdPartyId \
                                                           and d.deceased_dt is not null \
                                                               and c.BILLINGNUMBER = 1 \
                                                                   and a.thirdpartytype = '1012')"
            
    
    def date_processing(self, date_str: str) -> datetime:
        if date_str == '' or not date_str:
            return None

        if isinstance(date_str, datetime):
            try: 
                date_time_obj = date_str.to_pydatetime().date()
            except: 
                date_time_obj = date_str
        else: 
            date_time_obj = parse(date_str, fuzzy=False).date()
      
        return date_time_obj 

 
    def birth_date_rule(self, birth_date: datetime) -> str:
        """ line 81
        Maximum age allowed for an individual customer is 120 years
        """
        
        entity = 'individual'
        line = '81'

        days_in_year = 365.2425 
        
        import pdb
        pdb.set_trace()
        
        if str(birth_date) == 'NaT':
            return entity + ' ' +  line + ' - missing birthdate'
        else:
            try:
                age = int((date.today() - birth_date).days / days_in_year)
                
            except:
                try: 
                    birth_date = birth_date.date()
                    age = int((date.today() - birth_date).days / days_in_year)
                except:
                    return entity + ' ' +  line + ' - missing birthdate'

        # if age is larger than 120 or birth date is greater than current date, return error message
        if age > 120 or birth_date > date.today():
            return entity + ' ' + line + ' - invalid age'
        else:
            return ""


    def death_date_rule(self, death_date: datetime, birth_date: datetime) -> list:
         """ line 82
         DECEASED_DATE must be equal to or greater than DATE_OF_BIRTH
         """
        
         entity = 'individual'
         line = '82'
         
         # if deseased date is smaller than birth date or greater than current date, return error message
         if death_date is not None and (death_date < birth_date or death_date > date.today()):
             return entity + ' ' + line + ' - deceased date is invalid'
         else:
             return ""



    def run(self):
        
        cx_Oracle.init_oracle_client(lib_dir='instantclient_19_8')
        # connect to IODSP
        self.or_con = cx_Oracle.connect(
                            'MZFT7N',
                            'Wxzy*11251991',
                            '{}:{}/{}'.format('dfw1lepdb421-scan.ally.corp','2030','IODSP',
                            encoding='UTF-8'))
        
        cur = self.or_con.cursor()
        
        
        # birth date profiling 
        cur.execute(self.sql_str_dob)
        column_names = [x[0] for x in cur.description]
        num_rows = 20000
        self.violations_df_dob = pd.DataFrame()
        while True:
            rows = cur.fetchmany(num_rows)
            if not rows:
                break    
            df=pd.DataFrame(rows)
            df.columns = column_names
            
            df["birth_date_obj"] = df[self.birth_date_column_name].apply(self.date_processing)
            df["birth_date_violations"] = df.apply(lambda a: self.birth_date_rule(a['birth_date_obj']), axis=1)
            
            df_violations = df[df['birth_date_violations'] != ""]
            self.violations_df_dob = pd.concat([self.violations_df_dob, df_violations], axis=0)
            print(len(self.violations_df_dob))
        
        
        # death date profiling
        cur.execute(self.sql_str_dod)
        column_names = [x[0] for x in cur.description]
        num_rows = 20000
        self.violations_df_dod = pd.DataFrame()       
        while True:

            rows = cur.fetchmany(num_rows)
            if not rows:
                break    
            df=pd.DataFrame(rows)
            df.columns = column_names
            
            df['death_date_obj'] = df[self.death_date_column_name].apply(self.date_processing)
            df["birth_date_obj"] = df[self.birth_date_column_name].apply(self.date_processing)
            
            df["death_date_violations"] = df.apply(lambda a: self.death_date_rule(a['death_date_obj'], a['birth_date_obj']), axis=1)
            
            df_violations = df[df['death_date_violations'] != ""]
            self.violations_df_dod = pd.concat([self.violations_df_dod, df_violations], axis=0)
            print(len(self.violations_df_dod))
        
        
        self.violations_df_dob = self.violations_df_dob.set_index('THIRDPARTYID')
        self.violations_df_dod = self.violations_df_dod.set_index('THIRDPARTYID')
        self.combined_df = pd.concat([self.violations_df_dob, self.violations_df_dod], axis=0)
        self.combined_df = self.combined_df[(self.combined_df['birth_date_violations'] != "") | (self.combined_df['death_date_violations'] != "")]

if __name__ == "__main__":
    Individual_Validation = Individual_Entity_Validation()
    Individual_Validation.run()

    Individual_Validation.combined_df.to_csv("Individual_Violations.csv")

   

        
        