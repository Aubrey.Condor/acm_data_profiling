# .
#!/usr/bin/env python3. 
# -*- coding: utf-8 -*-
"""
Created on Tue Nov  8 10:13:53 2022

@author: mzft7n
"""

import pandas as pd
import re
import cx_Oracle

from utils import create_specialchar_list


class Address:
    """Includes validation rules for Address entities: postal code."""
        
    def __init__(
        self, 
        city_column_name: str = "ADDRESSLINE3",
        country_column_name: str = "COUNTRYCODE",
        line_one_column_name: str = 'ADDRESSLINE1',
        line_two_column_name: str = "ADDRESSLINE2",
        postal_code_column_name: str = "POSTALCODE",
        state_code_column_name: str = "ADDRESSLINE4"
        ) -> None:
        
        # import zip code mapping          
        try:
            self.state_zip_mapping =pd.read_csv('state_zipcode_mapping.csv').rename(columns={
                'ZIPCODE RANGE /ZIP CODE STARTS WITH':'zip_range'})
        except FileNotFoundError:
            raise Exception("File not found!")
        
        # import data lookup table    
        try:
            self.lookup_df = pd.read_csv('data_model_lookup.csv')
        except FileNotFoundError:
            raise Exception("File not found!")

        self.city_column_name = city_column_name
        self.country_column_name = country_column_name
        self.postal_code_column_name = postal_code_column_name
        self.state_code_column_name = state_code_column_name
        self.line_one_column_name = line_one_column_name
        self.line_two_column_name = line_two_column_name
        
        self.sql_str = "With tbl1 as (Select a.agrmnt_id from IODS.IODS_SCHED_DERIVED_ATTR_V a \
                            Where a.AGRMNT_STATUS in ('Active', 'TerminationInProgress') and a.agrmnt_id not in (1,2)), \
                        tbl2 as (Select a.agrmnt_id, b.EXPCUSID, b.INVCUSID from tbl1 a, ALFA_ODS.odsAgreement b Where a.agrmnt_id = b.id), \
                        tbl3 as (Select distinct b.AGREEMENTID, b.GUARANTORID from tbl1 a, ALFA_ODS.odsSecurity b Where a.agrmnt_id = b.AGREEMENTID \
                                 and b.SECURITYTYPENAME in ( 'Guarantor','Cobuyer','Colessee','Spouse','Owner','Other Owner','Previous Buyer',\
                                                            'Previous Co-Buyer','Line of Credit Guarantor','Previous Co-Lessee','Previous Guarantor',\
                                                                'Previous Lessee')), \
                        tbl4 as (Select distinct agrmnt_id, EXPCUSID as ThirdPartyId,1 from tbl2 union \
                                 Select distinct agrmnt_id, INVCUSID,2  from tbl2 union \
                                 Select AGREEMENTID, GUARANTORID,3 from tbl3) \
                        select distinct thirdpartyid, addressline1, addressline2, addressline3, addressline4, \
                            addresstype, postalcode, countrycode \
                                from  (Select b.agrmnt_id, b.ThirdPartyId, a.THIRDPARTYTYPE, a.NATIONALIDNUMBER, \
                                       c.addressline1, c.addressline2, c.addressline3, c.addressline4, \
                                           c.addressline5, c.addresstype, c.postalcode, c.countrycode \
                                               from alfa_ods.odsThirdParty a, tbl4 b, alfa_ods.odsBillingAddress c \
                                                   Where b.ThirdPartyId = a.id \
                                                       and  b.ThirdPartyId = c.THIRDPARTYID \
                                                           and c.BILLINGNUMBER = 1 \
                                                           and a.thirdpartytype = '1012')"
        
    numeric_regex = '^[0-9]+$'
    alpha_regex = "^[a-zA-Z]+$"
    alphanumeric_regex = "^[a-zA-Z0-9]+$"
    
        
    def postal_code1(self, postal_code: str, country: str) -> list:
        """ Line 2.
        
        US address Zip codes must be in either 'NNNNN' or 'NNNNN-NNNN' format. 
        Can contain numeric and only  '-' character
        
        When Country = US
        Applies to customers for all address types in ACM.
        
        line 3
        Zip codes must have a hyphen(-) for Zip + 4 Postal Codes.
        """
        entity = 'postal_code'
        line = ['2', '3']
        
        if country == 'US' or country == 'USA':
            
            if len(postal_code) == 5:
                if not re.search(self.numeric_regex, postal_code):
                    return entity + ' ' + line[0] + ' - non-numeric value found'
                
            elif len(postal_code) == 10:
             
                if postal_code[5] != '-':
                    return entity + ' ' + line[1] + ' - incorrect format'
                
                elif not re.search(self.numeric_regex, postal_code.replace('-','')):
                    return entity + ' ' + line[0] + ' - non-numeric value found'
            else:
                return entity + ' ' + line[0] + ' - invalid format'
        
        return ""
            
            
    def postal_code2(self, postal_code: str, country: str) -> list:
        """ Line 4.
        Zip codes for the countries except US,Canada and Portgal can 
        contain alphanumeric and '-' characters
        
        "When Country  <>  US, CA, PT
        Applies to customers for all address types in ACM "
        """   
        
        entity = 'postal_code'
        line = '4'
            
        if country not in ['US', 'CA', 'PT', 'USA', 'CAN', 'PRT']:
            if not re.search(self.alphanumeric_regex, postal_code.replace('-', '')):
                return entity + ' ' + line + ' - non-alphanumeric value found'

        return ""
            
            
    def postal_code3(self, postal_code: str, country: str) -> list:
        """ Line 5.
        Canada address zipcodes must be in ANA NAN format. Can contain 
        Alphanumeric and space characters.
        
        "When Country = CA
        Applies to customers for all address types in ACM "
        """
        
        entity = 'postal_code'
        line = '5'

        if country == 'CA' or country =='CAN':

            if len(postal_code) != 7:
                return entity + ' ' + line + ' - incorrect format'
            elif (not re.search(self.alpha_regex, postal_code[0])) or not re.search(self.numeric_regex, postal_code[1]) or not re.search(self.alpha_regex, postal_code[2]) or \
                postal_code[3] != ' ' or not re.search(self.numeric_regex, postal_code[4]) or not re.search(self.alpha_regex, postal_code[5]) or not re.search(self.numeric_regex, postal_code[6]):
                return entity + ' ' + line + ' - incorrect format'

        return ""
        
            
    def postal_code4(self, postal_code: str, country: str) -> list:
        """ Line 6.
        
        Portugal address zipcodes must be in NNNN format. 
        Can contain only numeric characters.
        
        Where Country = PT
        Applies to customers for all address types in ACM 
        """
        
        entity = 'postal_code'
        line = '6'
            
        if country == 'PT' or country == 'PRT':
            if not re.search(self.numeric_regex, postal_code) or len(postal_code) != 4:
                return entity + ' ' + line + ' - incorrect format for Portugal'

        return ""
            
        
    def postal_code5(self, postal_code: str, country: str, state_code: str) -> list:
         
        #Line 7.
        """
        US address ZipCode must reside within the specific assigned zipcode
        range for  State (or province).  Refer to 'State and Zipcode Mapping' 
        worksheet.
        
        When Country  = US.
        Applies to customers for all address types in ACM.
        """
        entity = 'postal_code'
        line = '7'
        
        state_zip_mapping = self.state_zip_mapping
        
        if country == 'US' or country == 'USA':
            try:
                zip_range = state_zip_mapping[(state_zip_mapping['COUNTRY'] == 'US') & (state_zip_mapping['STATE'] == state_code)]['zip_range'].item()
            except:

                return entity + ' ' + line + ' - not in state mapping'
            
            zip_range_list = [[int(x)for x in x.split('~')] for x in zip_range.split('|')]
            num_ranges = len(zip_range_list)
            
            if len(postal_code) == 5:
                postal_int = int(postal_code)
            else:
                try:
                    postal_int = int(postal_code[:5])
                except:
                    return entity + ' ' + line + ' - cannot check state range'

            
            for i in range(num_ranges):
                low_zip = zip_range_list[i][0]
                high_zip = zip_range_list[i][1]
                
                if (postal_int <= high_zip) and (postal_int >= low_zip):
                    return ""

            return entity + ' ' + line + ' - out of state range'

        return ""
        
                
    def postal_code6(self, postal_code: str, country: str, state_code: str) -> list:
        """ Line 8.
        
        Canada address ZipCode must start with specific predifined character(s)
        for State (or province).  Refer to 'State and Zipcode Mapping' 
        worksheet. 
        
        When Country = CA.
        Applies to customers for all address types in ACM.

        Returns
        -------
        (entity, line, reason)

        """
        entity = 'postal_code'
        line = '8'
        
        state_zip_mapping = self.state_zip_mapping            
            
        if country == 'CA' or country == 'CAN':
            try:
                zip_prefix = state_zip_mapping[(state_zip_mapping['COUNTRY'] == 'CA') 
                                              & (state_zip_mapping['STATE'] == state_code)
                                              ]['zip_range'].item()
            except:
                return entity + ' ' + line + ' - incorrect zip prefix'
            
            zip_prefix_list = zip_prefix.split(',')
            
            if not postal_code[0] in zip_prefix_list:
                return entity + ' ' + line + ' - incorrect zip prefix'

        return ""
        
            
    def postal_code7(self, postal_code: str) -> list:
        """Line 20.
        
        Length of the ZIP attribute should be <= 10.
        
        Applies to customers for all address types in ACM.
        
        Returns
        -------
        (entity, line, reason)

        """
        entity = 'postal_code'
        line = '20'
                
        if len(postal_code) > 10:
            return entity + ' ' + line + ' - length > 10'

        return ""


    def country1(self, country: str) -> list:
        """Line 9.
        
        Country codes associated to any type of address must be a value in the 
        ACM's country reference listing (eg: US, MX, CA etc).
        
        Applies to customers for all address types in ACM.
        
        Returns
        -------
        (entity, line, reason)
        
        """
        
        entity = 'country'
        line = '9'
        
        lookup_df = self.lookup_df
        country_codes_list = [x for x in lookup_df[lookup_df['Lookups'] == 'Country']['Code']]
        
        if country not in country_codes_list:
            return entity + ' ' + line + ' - invalid country code'
        
        return ""
        
            
    def country2(self, country: str, line_one: str) -> list:
        """Line 11.
        
        Country is required for an address when address is provided.
        
        Applies to customers for all address types in ACM.
        
        Returns
        -------
        (entity, line, reason).

        """
        
        entity = 'country'
        line = '11'
        
        if len(line_one) > 0 and len(country) == 0:
            return entity + ' ' + line + ' - missing country'
        
        return ""
        
        
    def state1(self, state_code: str, line_one: str) -> list:
        """Line 10.
        
        State is required for an address when address is provided.
        
        Applies to customers for all address types in ACM.

        Returns
        -------
        (entity, line, reason).

        """
        
        entity = 'state'
        line = '10'
        
        if len(line_one) > 0 and len(state_code) == 0:
            return entity + ' ' + line + ' - missing state'
        
        return ""
            
            
            
    def city1(self, city: str, line_one: str) -> list:
        """Line 13.
        
        City is required when address is provided.
        
        Applies to customers for all address types in ACM.

        Returns
        -------
        (entity, line, reason).

        """
        
        entity = 'city'
        line = '13'
        
        if len(line_one) > 0 and len(city) == 0:
            return entity + ' ' + line + ' - missing city'
        
        return ""


    def city2(self, city: str) -> list:
        """Line 19.
        
        City is required when address is provided.
        
        Applies to customers for all address types in ACM.

        Returns
        -------
        (entity, line, reason).

        """
        
        entity = 'city'
        line = '13'
        
        if len(city) > 0 and len(city) == 0:
            return entity + ' ' + line + ' - missing city'
        
        return ""
        
        
    def city3(self, city: str) -> list:
        """Line 23.
        
        Address  can contain alphanumeric and these special characters:
            (see special characters list in utils.py)

        Returns
        -------
        (entity, line, reason).

        """
        
        entity = 'city'
        line = '13'
        
        special_chars_list = [x[1] for x in create_specialchar_list()]
        
        # check if anything other than alphanumeric:
        if not re.search(self.alphanumeric_regex, city):
            # if we enter here, something is not alphanumeric. check for valid char list
            for char in city:
                # find which character is not alphanumeric
                if not re.search(self.alphanumeric_regex, char):
                    # if we enter here, char is not alpha numeric.
                    # check if non-alphanumeric char is in list of acceptable chars
                    if not char.encode('utf-8') in special_chars_list:
                        return entity + ' ' + line + ' - invalid character'
        
        return ""
    
    """ 
    def address_type(self, address_type: str) -> list:
        Line 22.
        
        "Must adhere to values in reference data table and are required values
        (eg: PRIMARY_RESIDENCE, MAILING etc)".
        
        Applies to customers for all address types in ACM.
        
        Returns
        -------
        (entity, line, reason)
        
        
        
        entity = 'address type'
        line = '22'
        
        lookup_df = self.lookup_df
        country_codes_list = [x for x in lookup_df[lookup_df['Lookups'] == 'Country']['Code']]
        
        if country not in country_codes_list:
            return entity + ' ' + line + ' - invalid country code'
        
        return ""
    """        
    
    
    def run(self):
        
        cx_Oracle.init_oracle_client(lib_dir='instantclient_19_8')
        # connect to IODSP
        self.or_con = cx_Oracle.connect(
                            'MZFT7N',
                            'Wxzy*11251991',
                            '{}:{}/{}'.format('dfw1lepdb421-scan.ally.corp','2030','IODSP',
                            encoding='UTF-8'))
        
        cur = self.or_con.cursor()
        
        cur.execute(self.sql_str)
        column_names = [x[0] for x in cur.description]
        num_rows = 20000
        self.violations_df = pd.DataFrame()
        print('here')
        
        while True:
            rows = cur.fetchmany(num_rows)
            if not rows:
                break    
            df=pd.DataFrame(rows)
            df.columns = column_names
    
            df['postal_code_obj'] = df[self.postal_code_column_name].apply(lambda x: str(x).strip())
            df['country_obj'] = df[self.country_column_name].apply(lambda x: str(x).strip().upper())
            df['state_code_obj'] = df[self.state_code_column_name].apply(lambda x: str(x).strip().upper())
            df['city_obj'] = df[self.city_column_name].apply(lambda x: str(x).strip().upper())
            df['line_one_obj'] = df[self.line_one_column_name].apply(lambda x: str(x))
            df['line_two_obj'] = df[self.line_two_column_name].apply(lambda x: str(x))
            
            df['postalcode1_violations'] = df.apply(lambda x: self.postal_code1(x['postal_code_obj'], x['country_obj']), axis=1)
            df['postalcode2_violations'] = df.apply(lambda x: self.postal_code2(x['postal_code_obj'], x['country_obj']), axis=1)
            df['postalcode3_violations'] = df.apply(lambda x: self.postal_code3(x['postal_code_obj'], x['country_obj']), axis=1)
            df['postalcode4_violations'] = df.apply(lambda x: self.postal_code4(x['postal_code_obj'], x['country_obj']), axis=1)
            df['postalcode5_violations'] = df.apply(lambda x: self.postal_code5(x['postal_code_obj'], x['country_obj'], x['state_code_obj']), axis=1)
            df['postalcode6_violations'] = df.apply(lambda x: self.postal_code6(x['postal_code_obj'], x['country_obj'], x['state_code_obj']), axis=1)
            df['postalcode7_violations'] = df.apply(lambda x: self.postal_code7(x['postal_code_obj']), axis=1)
            df['country2_violations'] = df.apply(lambda x: self.country2(x['country_obj'], x['line_one_obj']), axis=1)
            df['state1_violations'] = df.apply(lambda x: self.state1(x['state_code_obj'], x['line_one_obj']), axis=1)
            df['city1_violations'] = df.apply(lambda x: self.city1(x['line_one_obj'], x['city_obj']), axis=1)
            df['city2_violations'] = df.apply(lambda x: self.city2(x['city_obj']), axis=1)
            df['city3_violations'] = df.apply(lambda x: self.city3(x['city_obj']), axis=1)
    
            df_violations = df[(df['postalcode1_violations'] != "") | 
                               (df['postalcode2_violations'] != "") | 
                               (df['postalcode3_violations'] != "") | 
                               (df['postalcode4_violations'] != "") | 
                               (df['postalcode5_violations'] != "") | 
                               (df['postalcode6_violations'] != "") | 
                               (df['postalcode7_violations'] != "") | 
                               (df['country2_violations'] != "") | 
                               (df['state1_violations'] != "") | 
                               (df['city1_violations'] != "") | 
                               (df['city2_violations'] != "") | 
                               (df['city3_violations'] != "")]
        

            self.violations_df = pd.concat([self.violations_df, df_violations], axis=0)
            print(len(self.violations_df))
        
                
if __name__ == "__main__":
    Address_Validation = Address()
    
    Address_Validation.run()
    Address_Validation.violations_df.to_csv('Address_Violations.csv')


    

                
                
        
        
        
    
    

