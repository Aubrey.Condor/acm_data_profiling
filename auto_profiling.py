# .
#!/usr/bin/env python3. 
# -*- coding: utf-8 -*-
"""
Created on Tue Nov  8 10:13:53 2022

@author: mzft7n
"""

import cx_Oracle
import pandas as pd
import re

cx_Oracle.init_oracle_client(lib_dir='instantclient_19_8')
or_con = cx_Oracle.connect(
                    'MZFT7N',
                    'Wxzy*4567',
                    '{}:{}/{}'.format('dfw1lvpdb251-sc.ally.corp','2000','AFDWP',
                    encoding='UTF-8'))
#connection.outputtypehandler = output_type_handler 


# Name, Email, BirthDt, TaxID, 
#sql_str = 'select * from FIN_DW.INDIVIDUAL where rownum <= 100'

sql_str = 'select * from FIN_DW.POSTAL_ADDRESS where rownum <= 100'


def oracle_result_df(sql_str):

    cur = or_con.cursor()
    cur.execute(sql_str)
    rows = cur.fetchall()
    columns = [x[0] for x in cur.description]
    
    df=pd.DataFrame(rows)
    df.columns = columns
    
    return df

# just for some sample data to play with
address_df = oracle_result_df(sql_str)


class Address:
    
    # format will be (entity, reason)
    violations_list = []
    
    def __init__(self, 
                 address_type,
                 city,
                 country,
                 beg_date,
                 end_date,
                 line_one,
                 line_two, 
                 line_three,
                 postal_code,
                 state_code,
                 residence_type):
        
        self.address_type = address_type
        self.city = city
        self.country = country
        self.beg_date = beg_date
        self.end_date = end_date
        self.line_one = line_one
        self.line_two = line_two
        self.line_three = line_three
        self.postal_code = postal_code
        self.state_code = state_code
        self.residence_type = residence_type
    
    numeric_regex = '^[0-9]+$'
    alpha_regex = "^[a-zA-Z]+$"
    alphanumeric_regex = "^[a-zA-Z0-9]+$"
    
    state_zip_mapping = pd.read_csv('state_zipcode_mapping.csv').rename(columns={
        'ZIPCODE RANGE /ZIP CODE STARTS WITH':'zip_range'})
    
    
    def postal_code1(self):
        """ Line 2.
        
        US address Zip codes must be in either 'NNNNN' or 'NNNNN-NNNN' format. 
        Can contain numeric and only  '-' character
        
        When Country = US
        Applies to customers for all address types in ACM.
        
        line 3
        Zip codes must have a hyphen(-) for Zip + 4 Postal Codes.
        """
        entity = 'postal_code'
        line = '3'
        
        postal_code = self.postal_code.strip()
        country = self.country.strip().upper()
        
        if country == 'US':
            
            if len(postal_code) == 5:
                if not re.search(self.numeric_regex, postal_code):
                    return (entity, 'non-numeric value found')
                
            elif len(postal_code) == 10:
             
                if postal_code[5] != '-':
                    return (entity, 'incorrect format')
                
                elif not re.search(self.numeric_regex, postal_code.replace('-'),''):
                    return (entity, 'non-numeric value found')
            else:
                return (entity, line, 'invalid length')
            
            
    def postal_code2(self):
        """ Line 4.
        Zip codes for the countries except US,Canada and Portgal can 
        contain alphanumeric and '-' characters
        
        "When Country  <>  US, CA, PT
        Applies to customers for all address types in ACM "
        """   
        
        entity = 'postal_code'
        line = '4'
        
        postal_code = self.postal_code.strip()
        country = self.country.strip().upper()
            
        if country not in ['US', 'CA', 'PT']:
            if not re.search(self.alphanumeric_regex, postal_code.replace('-', '')):
                return (entity, line, 'non-alphanumeric value found')
            
            
    def postal_code3(self):
        """ Line 5.
        Canada address zipcodes must be in ANA NAN format. Can contain 
        Alphanumeric and space characters.
        
        "When Country = CA
        Applies to customers for all address types in ACM "
        """
        
        entity = 'postal_code'
        line = '5'
        
        postal_code = self.postal_code.strip()
        country = self.country.strip().upper()
            
        if country == 'CA':
            if (not re.search(self.alpha_regex, postal_code[0])) or not re.search(self.numeric_regex, postal_code[1]) or not re.search(self.alpha_regex, postal_code[2]) or \
            postal_code[3] != ' ' or not re.search(self.numeric_regex, postal_code[4]) or not re.search(self.alpha_regex, postal_code[5]) or not re.search(self.numeric_regex, postal_code[5]):
                return (entity, line, 'incorrect format')
        
            
    def postal_code4(self):
        """ Line 6.
        
        Portugal address zipcodes must be in NNNN format. 
        Can contain only numeric characters.
        
        Where Country = PT
        Applies to customers for all address types in ACM 
        """
        
        entity = 'postal_code'
        line = '6'
        
        postal_code = self.postal_code.strip()
        country = self.country.strip().upper()
            
        if country == 'PT':
            if not re.search(self.numeric_regex, postal_code):
                return (entity, line, 'non-numeric value found')
            
            
    def postal_code5(self):
        """ Line 7.
        
        US address ZipCode must reside within the specific assigned zipcode
        range for  State (or province).  Refer to 'State and Zipcode Mapping' 
        worksheet.
        
        When Country  = US.
        Applies to customers for all address types in ACM.
        """
        entity = 'postal_code'
        line = '7'
        
        postal_code = self.postal_code.strip()
        country = self.country.strip().upper()
        state_code = self.state_code.strip().upper()
        state_zip_mapping = self.sate_zip_mapping
        
        if country == 'US':
            zip_range = state_zip_mapping[(state_zip_mapping['COUNTRY'] == 'US') 
                                          & (state_zip_mapping['STATE'] == state_code)
                                          ]['zip_range'].item()
            
            zip_range_list = [[int(x)for x in x.split('~')] for x in zip_range.split('|')]
            num_ranges = len(zip_range_list)
            
            postal_int = int(postal_code)
            
            for i in range(num_ranges):
                low_zip = zip_range_list[i][0]
                high_zip = zip_range_list[i][1]
                
                if not (postal_int < high_zip) or (postal_int > low_zip):
                    return (entity, line, 'out of state range')
               
                
    def postal_code7(self):
        """ Line 8.
        
        Canada address ZipCode must start with specific predifined character(s)
        for State (or province).  Refer to 'State and Zipcode Mapping' 
        worksheet. 
        
        When Country = CA.
        Applies to customers for all address types in ACM.

        Returns
        -------
        (entity, line, reason)

        """
        entity = 'postal_code'
        line = '8'
        
        postal_code = self.postal_code.strip()
        country = self.country.strip().upper()
        state_code = self.state_code.strip().upper()
        state_zip_mapping = self.sate_zip_mapping            
            
        if country == 'CA':
            zip_prefix = state_zip_mapping[(state_zip_mapping['COUNTRY'] == 'CA') 
                                          & (state_zip_mapping['STATE'] == state_code)
                                          ]['zip_range'].item()
            
            zip_prefix_list = zip_prefix.split(',')
            
            if not postal_code[0] in zip_prefix_list:
                return (entity, line, 'incorrect zip prefix')
            
            
            
            
            
            
            
            
            
            
            
            
            
            
        
        
    
    

